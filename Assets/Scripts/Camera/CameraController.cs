﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public float m_velocity = 50f; 
	public int m_tabSize = 50;

	void Start () {

	}

	void Update () {           
		UpdateCameraPosition();
		UpdateCameraHeight();
	}

	// For updating camera position
	private void UpdateCameraPosition()
	{
		Vector3 mouse = Input.mousePosition;
		Vector3 direction = Vector3.zero;

		if (Screen.width - m_tabSize < mouse.x)
			direction.x = 1f - ((Screen.width - mouse.x) / m_tabSize);
		else if (m_tabSize > mouse.x)
			direction.x = -1f + (mouse.x / m_tabSize);

		if (Screen.height - m_tabSize < mouse.y)
			direction.z = 1f - ((Screen.height - mouse.y) / m_tabSize);
		else if (m_tabSize > mouse.y)
			direction.z = -1f + (mouse.y / m_tabSize);

		transform.Translate(m_velocity * direction * Time.deltaTime, Space.World);  
	}

	private void UpdateCameraHeight()
	{

	}

}
